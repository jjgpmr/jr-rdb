/*
-- Query: SELECT * FROM rdb.client
LIMIT 0, 1000

-- Date: 2022-07-22 15:58
*/
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (1,'José Rosa','123456789','5555','1989-09-14','225108389','916830604','maiarosajose@gmail.com','devop','1',NULL);
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (2,'Ana Magalhães','2222222','8888','2000-01-23','225104658','916830546','anam@gmail.com','sales','2',NULL);
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (6,'Osvaldo','222111333','4444','1982-06-12','222555888','999888555','o@gmail.com','poney','3',NULL);
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (8,'Alex Sandro','222555444','8888','1945-06-02','224666888','999222000','a@gmail.com','football','4',NULL);
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (10,'Miguel Oliveira','566321456','6666','1989-02-12','222222222','999999999','m@gmail.com','motos','5',NULL);
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (11,'Luis carlos','888999777','5555','1987-02-02','222555888','999888777','l@gmail.com','none','6',NULL);
INSERT INTO `` (`id`,`name`,`nif`,`password`,`dob`,`telephone`,`mobile`,`email`,`occupation`,`contract_number`,`user_type`) VALUES (12,'Pedro Morais','222666555','8989','1985-06-02','888555444','666555777','m@gmail.com','lutador','7',NULL);
