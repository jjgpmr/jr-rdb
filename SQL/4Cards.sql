/*
-- Query: SELECT * FROM rdb.cards
LIMIT 0, 1000

-- Date: 2022-07-22 15:59
*/
INSERT INTO `` (`Id`,`card_number`,`card_type`,`id_client`,`id_account`,`number_daily_withdrawals`,`amount_daily_withdrawals`,`pin`,`first_time_login`,`plafond`) VALUES (9,1,'d',2,4,NULL,0000000000000000000400,'1234',NULL,NULL);
INSERT INTO `` (`Id`,`card_number`,`card_type`,`id_client`,`id_account`,`number_daily_withdrawals`,`amount_daily_withdrawals`,`pin`,`first_time_login`,`plafond`) VALUES (10,2,'c',1,1,0000000020,NULL,'5678',NULL,NULL);
INSERT INTO `` (`Id`,`card_number`,`card_type`,`id_client`,`id_account`,`number_daily_withdrawals`,`amount_daily_withdrawals`,`pin`,`first_time_login`,`plafond`) VALUES (12,5647,'c',11,16,0000000000,NULL,'2022',1,NULL);
INSERT INTO `` (`Id`,`card_number`,`card_type`,`id_client`,`id_account`,`number_daily_withdrawals`,`amount_daily_withdrawals`,`pin`,`first_time_login`,`plafond`) VALUES (18,2589,'c',11,16,0000000000,NULL,'7425',1,200);
INSERT INTO `` (`Id`,`card_number`,`card_type`,`id_client`,`id_account`,`number_daily_withdrawals`,`amount_daily_withdrawals`,`pin`,`first_time_login`,`plafond`) VALUES (19,1276,'c',8,10,0000000000,NULL,'4743',1,200);
INSERT INTO `` (`Id`,`card_number`,`card_type`,`id_client`,`id_account`,`number_daily_withdrawals`,`amount_daily_withdrawals`,`pin`,`first_time_login`,`plafond`) VALUES (20,5763,'c',6,12,0000000000,NULL,'6785',1,150);
