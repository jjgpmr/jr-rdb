/*
-- Query: SELECT * FROM rdb.transaction
LIMIT 0, 1000

-- Date: 2022-07-22 16:00
*/
INSERT INTO `` (`id`,`date_of_transaction`,`amount_of_transaction`,`original_account`,`destination_account`) VALUES (1,'2022-03-25',10,1,2);
INSERT INTO `` (`id`,`date_of_transaction`,`amount_of_transaction`,`original_account`,`destination_account`) VALUES (2,'2022-04-12',20,1,4);
INSERT INTO `` (`id`,`date_of_transaction`,`amount_of_transaction`,`original_account`,`destination_account`) VALUES (3,'2022-03-25',35,4,2);
INSERT INTO `` (`id`,`date_of_transaction`,`amount_of_transaction`,`original_account`,`destination_account`) VALUES (4,'2022-03-25',35,4,2);
