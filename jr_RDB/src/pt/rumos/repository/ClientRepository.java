package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;

public interface ClientRepository {//INterfaces nunca podem mudar, pode se acrescentar, mas n�o apagar ou alterar m�tudos j� criados.
    // C R U D
    
    // Create (Criar)
    // Retrieve (Ler)
    // Update (Atualizar)
    // Delete (Apagar)


	Optional<Client> save(Client client);

    Optional<Client> update(Client client);

    List<Client> findAll();

    Optional<Client> findById(Long id);

    void deleteById(Long id);

    Optional<Client> findByNif(String nif);
    
    void deleteByNIF(String nif);
}
