package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import pt.rumos.console.ManagementApp;
import pt.rumos.model.Account;
import pt.rumos.model.Client;

public class AccountRepositorySQLImpl implements AccountRepository {//classe que utiliza uma Base de dados para armazenar dados, um aramzenamento f�sico, n�o na mem�ria.

	//ManagementApp app = ManagementApp.getInstance() ;
	
	@Override
	public Optional<Account> save(Account account) {
		
		
		String insertStatement = "INSERT INTO account(account_number, id_client, balance) "
				+ "VALUES ("
				+"'" + account.getAccountNumber() + "', "
				+"'" + account.getOwner().getId() + "', "
				+"'" + account.getBalance() + "' );";
				Database.execute(insertStatement);
				
				String selectStatement = "SELECT LAST_INSERT_ID();";
				ResultSet rs = Database.executeQuery(selectStatement);
				try {
					rs.next();
					account.setId(rs.getLong(1));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				return findByAccountNumber(account.getAccountNumber());
	}

	@Override
	public Optional<Account> update(Account account) {
		try {
			String updateAccountData = "UPDATE account 	 SET account_number = "+ account.getAccountNumber() + 
				", id_client = "+ account.getOwner().getId() + ", balance = "
				+ account.getBalance() +"   WHERE account.id = "+ account.getId() +";"; 
			Database.execute(updateAccountData);
			return Optional.of(account);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	@Override
	public List<Account> findAll() {
		ManagementApp app = ManagementApp.getInstance() ;
		String selectStatement = "SELECT * FROM rdb.account;";
		ResultSet rs = Database.executeQuery(selectStatement);
		List <Account> allAccounts = new ArrayList<>();
		try {
			while (rs.next()) {
				Account account = new Account();	
				account.setId(rs.getLong(1));
				account.setAccountNumber(rs.getInt(2));
				Client client = app.getClientById(rs.getLong(3));
				if (client == null) {
				}
				account.setOwner(client);
				account.setBalance(rs.getDouble(4));
				allAccounts.add(account);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allAccounts;
	}

	@Override
	public Optional<Account> findById(Long id) {
		
		Client owner;
		String selectStatement = "SELECT * FROM account WHERE id = " + id + ";";
		ResultSet rs = Database.executeQuery(selectStatement);
		try {
			while (rs.next()) {
				Account a = new Account();
				a.setId(rs.getLong(1));
				a.setAccountNumber(rs.getInt(2));
				ManagementApp app =  ManagementApp.getInstance();
				owner =app.getClientById(rs.getLong(3));
				a.setOwner(owner);
				a.setBalance(rs.getDouble(4));
				return Optional.of(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	@Override
	public void deleteById(Long id) {
		String deleteAccount = "DELETE FROM account WHERE id = " + id + ";";
		Database.execute(deleteAccount);

	}
//	private Optional<Account> extractObject(ResultSet rs){
//		 try {
//	            if (rs.next()) {
//	                Account account = buildObject(rs);
//	                return Optional.of(account);
//	            }
//	        } catch (SQLException e) {
//	            e.printStackTrace();
//	        }
//	        return Optional.empty();
//	}
//	
//	private Account buildObject(ResultSet rs) throws SQLException {
//		Account account = new Account();
//		account.setId(rs.getLong(1));
//		account.setAccountNumber(rs.getInt(2));
//		Long clientId = rs.getLong(3);
//		account.setBalance(rs.getDouble(4));
//		
//		String selectStatement = "SELECT * FROM client WHERE id = " + clientId + ";";
//		rs = Database.executeQuery(selectStatement);
//		
//		rs.next();
//		Client c = new Client();
//		c.setId(rs.getLong(1));
//        c.setName(rs.getString(2));
//        c.setNif(rs.getString(3));
//        c.setPassword(rs.getString(4));
//        c.setDob(rs.getDate(5).toLocalDate());
//        c.setTelephone(rs.getString(6));
//        c.setMobile(rs.getString(7));
//        c.setEmail(rs.getString(8));
//        c.setOccupation(rs.getString(9));
//		account.setOwner(c);
//		
//		selectStatement = "SELECT * FROM account_client, client WHERE id_account = " + account.getId() + " AND id_client = client.id;";
//		rs = Database.executeQuery(selectStatement);
//		List<Client> secondaryOwners = new ArrayList<Client>();
//		while (rs.next()) {
//            Client secondary = new Client();
//            secondary.setId(rs.getLong(3));
//            secondary.setName(rs.getString(4));
//            secondary.setNif(rs.getString(5));
//            secondary.setPassword(rs.getString(6));
//            secondary.setDob(rs.getDate(7).toLocalDate());
//            secondary.setTelephone(rs.getString(8));
//            secondary.setMobile(rs.getString(9));
//            secondary.setEmail(rs.getString(10));
//            secondary.setOccupation(rs.getString(11));
//            secondaryOwners.add(secondary);
//        }
//		account.setSecondaryOwners(secondaryOwners);
//		return account;
//	}

	@Override
	public Optional<Account> findByAccountNumber(Integer accountNumber) {
		String selectStatement = "SELECT * FROM rdb.account WHERE account_number = " + accountNumber + ";";
		ResultSet rs = Database.executeQuery(selectStatement);
		
		try {
			while(rs.next()) {
				Account account = new Account();
				account.setAccountNumber(rs.getInt(1));
				Long clientId = rs.getLong(2);
				account.setBalance(rs.getDouble(3));
				return Optional.of(account);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return Optional.empty();
	}

	@Override
	public void deleteByAccountNumber(Integer accountNumber) {
		String deleteAccount = "DELETE FROM account WHERE account_number = " + accountNumber.intValue() + ";";	
		Database.execute(deleteAccount);
	}

}
