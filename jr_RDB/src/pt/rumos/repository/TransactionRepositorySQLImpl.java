package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.console.ManagementApp;
import pt.rumos.model.Account;
import pt.rumos.model.Transaction;

public class TransactionRepositorySQLImpl implements TransactionRepository {

	@Override
	public Optional<Transaction> save(Transaction transaction) {
		
		System.out.println("Date Time" + transaction.getDateOfTransaction().toString());
		
		String insertStatement = "INSERT INTO transaction(date_of_transaction, amount_of_transaction, original_account, destination_account) "
		+ "VALUES ("
		+"'" + transaction.getDateOfTransaction().toString()+ "', "
	    + transaction.getAmountOfTransaction() + ", "
		+ transaction.getOriginalAccount().getId() + ", "
		+ transaction.getDestinationAccount().getId()+");";
		System.out.println(insertStatement);
		Database.execute(insertStatement);
		
		String selectStatement = "SELECT LAST_INSERT_ID();";
		ResultSet rs = Database.executeQuery(selectStatement);
		try {
			rs.next();
			transaction.setId(rs.getLong(1));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return findById(transaction.getId());
	}

//	@Override
//	public Optional<Transaction> update(Transaction transaction) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public List<Transaction> findAll() {
		
		ManagementApp app = ManagementApp.getInstance();
		String selectAllTransactions = "SELECT * FROM transaction;";
		ResultSet rs = Database.executeQuery(selectAllTransactions);
		List<Transaction> allTransactions = new ArrayList<>();
		try {
			while(rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong(1));
				System.out.println(rs.getDate(2));
				//t.setDateOfTransaction(LocalDateTime.
				Account acc = app.getAccountById(rs.getLong(4));
				t.setOriginalAccount(acc);
				Optional<Account> opAccount = Optional.of(app.getAccountById(rs.getLong(5)));
		        if (opAccount.isPresent()) {
		        	t.setDestinationAccount(opAccount.get());
		        }	
		        else	{
		        	t.setDestinationAccount(null);

		        }
				allTransactions.add(t);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allTransactions;
	}

	@Override
	public Optional<Transaction> findById(Long id) {
		
		String selectStatement = "SELECT * FROM transaction WHERE id = " + id + ";";
		ResultSet rs = Database.executeQuery(selectStatement);
		try {
			while (rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong(1));
				t.setDateOfTransaction(rs.getDate(2).toString());
				t.setAmountOfTransaction(rs.getDouble(3));
				t.setOriginalAccount(rs.getLong(4));    
				t.setDestinationAccount(rs.getLong(5));
				return Optional.of(t);
			}
	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Optional.empty();
	}

//	@Override
//	public void deleteById(Long id) {
//		String deleteTransaction = "DELETE FROM transaction WHERE id = " + id + ";";
//		Database.execute(deleteTransaction);

//	}

}
