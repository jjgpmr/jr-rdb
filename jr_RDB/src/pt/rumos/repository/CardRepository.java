package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Card;

public interface CardRepository {
	
	Optional<Card> save(Card card);
	
	Optional<Card> update(Card card);
	
	List<Card> findAll();
	
	Optional<Card> findById(Long id);
	
	void deleteById(Long id);
	
	Optional<Card> FindByCardNumber(String cardNumber);
	
	void deleteByCardNumber(String cardNumber);
}
