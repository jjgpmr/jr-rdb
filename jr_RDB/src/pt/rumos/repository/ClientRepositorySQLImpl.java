package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;

public class ClientRepositorySQLImpl implements ClientRepository {//classe que utiliza uma Base de dados para armazenar dados, um aramzenamento f�sico, n�o na mem�ria.

	@Override
	public Optional<Client> save(Client client) {
		
		String insertStatement = "INSERT INTO client(name, nif, password, dob, telephone, mobile, email, occupation, contract_number) "
				+ "VALUES ("
				+"'" + client.getName() + "', "
				+"'" + client.getNif() + "', "
				+"'" + client.getPassword() + "', "
				+"'" + client.getDob() + "', "
				+"'" + client.getTelephone() + "', "
				+"'" + client.getMobile() + "', "
				+"'" + client.getEmail() + "', "
				+"'" + client.getOccupation() + "'," 
				+"'" + client.getContractNumber() + "' );";
				Database.execute(insertStatement);
				
				String selectStatement = "SELECT LAST_INSERT_ID();";
				ResultSet rs = Database.executeQuery(selectStatement);
				try {
				rs.next();
				client.setId(rs.getLong(1));
				} catch(SQLException e) {
					e.printStackTrace();
				}
				return findByNif(client.getNif());
	}

	@Override
	public Optional<Client> update(Client client) {
		try { 
		String updateClientData = "UPDATE client SET name  " + client.getName() + ", nif " + client.getNif() + ", password " + client.getPassword()
					+", dob " + client.getDob() +", telephone " + client.getTelephone() +", mobile " + client.getMobile() +",email " + client.getEmail()
					+",occupation " + client.getOccupation() +", contact_number " + client.getContractNumber() 
					+ "WHERE id = " + client.getId() + ";";
		 Database.execute(updateClientData);
		return Optional.of(client);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}
	
	@Override
	public List<Client> findAll() {
		String selectAllClients = "SELECT * FROM rdb.client;";
		ResultSet rs = Database.executeQuery(selectAllClients);
		List<Client> allClients = new ArrayList<>();
		try {
			while (rs.next()) {
				Client c = new Client();
				c.setId(rs.getLong(1));
				c.setName(rs.getString(2));
				c.setNif(rs.getString(3));
				c.setPassword(rs.getString(4));
				c.setDob(rs.getDate(5).toLocalDate());
				c.setTelephone(rs.getString(6));
				c.setMobile(rs.getString(7));
				c.setEmail(rs.getString(8));
				c.setOccupation(rs.getString(9));
				c.setContractNumber(rs.getString(10));
				allClients.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allClients;
	}

	@Override
	public Optional<Client> findById(Long id) {
		String selectStatement = "SELECT * FROM rdb.client WHERE id = " + id + ";";
		ResultSet rs = Database.executeQuery(selectStatement);
		
		try {
			while (rs.next()) {
				Client c = new Client();
				c.setId(rs.getLong(1));
				c.setName(rs.getString(2));
				c.setNif(rs.getString(3));
				c.setPassword(rs.getString(4));
				c.setDob(rs.getDate(5).toLocalDate());
				c.setTelephone(rs.getString(6));
				c.setMobile(rs.getString(7));
				c.setEmail(rs.getString(8));
				c.setOccupation(rs.getString(9));
				c.setContractNumber(rs.getString(10));
				return Optional.of(c);//sendo id uma PrimaryKey, tendo sido assim criada a tabela no MySQL, podemos fazer o return do cliente em causa que estamos � procura pelo id.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return Optional.empty();
	}

	@Override
	public void deleteById(Long id) {
		String deleteClient = "DELETE FROM rdb.client WHERE id = " + id + ";";
		Database.execute(deleteClient);

	}

	@Override
	public Optional<Client> findByNif(String nif) { 
		String selectStatement = "SELECT * FROM rdb.client WHERE nif = " + nif + ";";
		ResultSet rs = Database.executeQuery(selectStatement);
		
		try {
			while (rs.next()) {
				Client c = new Client();
				c.setId(rs.getLong(1));
				c.setName(rs.getString(2));
				c.setNif(rs.getString(3));
				c.setPassword(rs.getString(4));
				c.setDob(rs.getDate(5).toLocalDate());
				c.setTelephone(rs.getString(6));
				c.setMobile(rs.getString(7));
				c.setEmail(rs.getString(8));
				c.setOccupation(rs.getString(9));
				c.setContractNumber(rs.getString(10));
				return Optional.of(c);//sendo nif uma constraint(Unique), tendo sido assim criada a tabela no MySQL, podemos fazer o return do cliente em causa que estamos � procura pelo nif.
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return Optional.empty();
	}

	@Override
	public void deleteByNIF(String nif) {
		String deleteClient = "DELETE FROM rdb.client WHERE nif = " + nif + ";";
		Database.execute(deleteClient);

	}

}
