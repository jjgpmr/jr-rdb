package pt.rumos.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;

public class ClientRepositoryListImpl implements ClientRepository {//classe que utiliza estrutura de dados para armazenar dados, neste caso ArrayList.

    private List<Client> clients = new ArrayList<Client>();

    @Override
    public Optional<Client> save(Client client) {
    	String insertStatement = "INSERT INTO client("+"name, "+"nif, "+"password, "+"dob, "+"phone, "+"mobile, "+"email, "+"occupation) "
				+ "VALUES ("
				+"'" + client.getName() + "', "
				+"'" + client.getNif() + "', "
				+"'" + client.getPassword() + "', "
				+"'" + client.getDob() + "', "
				+"'" + client.getTelephone() + ", "
				+"'" + client.getMobile() + ", "
				+"'" + client.getEmail() + ", "
				+"'" + client.getOccupation() + "' );";
    	return null;
    }

    @Override
    public Optional<Client> update(Client client) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Client> findAll() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<Client> findById(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteById(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteByNIF(String nif) {

    }

    @Override
    public Optional<Client> findByNif(String nif) {
        return null;
    }

}
