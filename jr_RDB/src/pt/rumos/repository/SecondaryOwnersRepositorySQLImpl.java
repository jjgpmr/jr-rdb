package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.console.ManagementApp;
import pt.rumos.model.Account;
import pt.rumos.model.Client;

public class SecondaryOwnersRepositorySQLImpl implements SecondaryOwnersRepository {

	@Override
	public Optional<Client> save(Long id_client, Long id_account) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<Client> update(Long id_client, Long id_account, Long new_id_client, Long new_id_account) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void findAll() {
		ManagementApp app = ManagementApp.getInstance() ;
		String selectStatement = "SELECT * FROM secondary_owner;";
		ResultSet rs = Database.executeQuery(selectStatement);
		
		try {
			while (rs.next()) {
				Client client = app.getClientById(rs.getLong(2));
				Account account = app.getAccountById(rs.getLong(3));
				account.addSecondaryOwner(client);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteByIds(Long id_client, Long id_account) {
		// TODO Auto-generated method stub

	}

}
