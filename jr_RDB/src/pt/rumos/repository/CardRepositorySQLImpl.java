package pt.rumos.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.console.ManagementApp;
import pt.rumos.model.Account;
import pt.rumos.model.Card;
import pt.rumos.model.Client;
import pt.rumos.model.CreditCard;
import pt.rumos.model.DebitCard;

public class CardRepositorySQLImpl implements CardRepository {

	@Override
	public Optional<Card> save(Card card) {

		if (card instanceof DebitCard)
		{
			DebitCard dCard = (DebitCard)card;


			String insertStatement = "INSERT INTO cards(card_number,card_type, id_client, id_account, number_daily_withdrawals, amount_daily_withdrawals, pin, first_time_login, plafond) "
					+ "VALUES ("
					+"'" + dCard.getCardNumber() + "', "
					+"'d', " 
					+ dCard.getHolder().getId() + ", "
					+ dCard.getAccount().getId() + ", "
					+ "null, "
					+ dCard.getAmountOfDailyWithdrawals()  
					+ "'" + dCard.getPinNumber() + "',"
					+ dCard.getFirstTimeLogin().intValue() + ", "
					+ "null);";
					Database.execute(insertStatement);	

			
			String selectStatement = "SELECT LAST_INSERT_ID();";
			ResultSet rs = Database.executeQuery(selectStatement);
			try {
				rs.next();
				card.setId(rs.getLong(1));
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return findById(card.getId());
		}	
		else {
			CreditCard cCard = (CreditCard)card;
			System.out.println("First Time: " + cCard.getFirstTimeLogin());
			String insertStatement = "INSERT INTO cards(card_number,card_type, id_client, id_account, number_daily_withdrawals, amount_daily_withdrawals, pin, first_time_login, plafond) "
					+ "VALUES ("
					+"'" + cCard.getCardNumber() + "', "
					+"'c', "
					+ cCard.getHolder().getId() + ", "
					+ cCard.getAccount().getId() + ", "
					+ cCard.getNumberOfDailyWithdrawals() + ", "
					+ "null, " 
					+ "'" + cCard.getPinNumber() + "'," 
					+ cCard.getFirstTimeLogin().intValue() + ", "
					+ cCard.getPlafond() + ");";
					Database.execute(insertStatement);
					
			String selectStatement = "SELECT LAST_INSERT_ID();";
			ResultSet rs = Database.executeQuery(selectStatement);
			try {
				rs.next();
				card.setId(rs.getLong(1));
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return findById(card.getId());
		}

	}

	@Override
	public Optional<Card> update(Card card) {



		return null;
	}

	@Override
	public List<Card> findAll() {
		ManagementApp app = ManagementApp.getInstance();
		String selectStatement = "SELECT * FROM cards;";
		ResultSet rs = Database.executeQuery(selectStatement);
		List<Card> allCards = new ArrayList<>();
		Card card;	
		try {
			while (rs.next()){
				char cardType =rs.getString(3).charAt(0);
				if(cardType =='c') {
					CreditCard creditCard = new CreditCard();
					creditCard.setNumberOfDailyWithdrawals(rs.getInt(6));
					card = creditCard;
				}else {
					DebitCard debitCard = new DebitCard();
					debitCard.setAmountOfDailyWithdrawals(rs.getDouble(7));
					card = debitCard;
				}

				card.setId(rs.getLong(1));
				card.setCardNumber(rs.getString(2));
				Client client = app.getClientById(rs.getLong(4));
				Account account = app.getAccountById(rs.getLong(5));
				card.setHolder(client);
				card.setAccount(account);
				card.setPinNumber(rs.getString(8));

				allCards.add(card);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return allCards;
	}

	@Override
	public Optional<Card> findById(Long id) {
		
		ManagementApp app = ManagementApp.getInstance();
		
		String selectStatement = "SELECT * FROM cards WHERE id = " + id + ";";
		ResultSet rs = Database.executeQuery(selectStatement);
		
			try {
				while(rs.next()) {
					Card card = new Card();
					card.setId(rs.getLong(1));
					card.setCardNumber(rs.getString(2));
					Client client = app.getClientById(rs.getLong(4));
					Account account = app.getAccountById(rs.getLong(5));
					card.setHolder(client);
					card.setAccount(account);
					card.setPinNumber(rs.getString(8));
					return Optional.of(card);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return Optional.ofNullable(null);
	}

	@Override
	public void deleteById(Long id) {
		String deleteCard = "DELETE FROM cards WHERE id = " + id + ";";
		Database.execute(deleteCard);

	}

	@Override
	public Optional<Card> FindByCardNumber(String cardNumber) {//testar
		
		ManagementApp app = ManagementApp.getInstance();
		
		String selectStatement = "SELECT * FROM cards WHERE card_number = " + cardNumber + ";";
		ResultSet rs = Database.executeQuery(selectStatement);

		try {
			while (rs.next()) {
				Card card = new Card();
				card.setId(rs.getLong(1));
				card.setCardNumber(rs.getString(2));
				Client client = app.getClientById(rs.getLong(4));
				Account account = app.getAccountById(rs.getLong(5));
				card.setHolder(client);
				card.setAccount(account);
				card.setPinNumber(rs.getString(8));
				return Optional.of(card);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(null);
	}

	

	@Override
	public void deleteByCardNumber(String cardNumber) {
		String deleteCard = "DELETE FROM cards WHERE card_number = " + cardNumber + ";";
		Database.execute(deleteCard);
	}

}
