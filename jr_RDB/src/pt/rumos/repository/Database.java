package pt.rumos.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {//classe que existe para servir de ponto de liga��o entre MySQL e projeto(aka jr-rdb)

	static String url = "jdbc:mysql://localhost:3306/rdb";
	static String username = "root";
	static String password = "124LfVgq";
	
	private static Connection conn;
	private static Statement stmt;
	
	private Database() {}
	
	private static void init() throws SQLException {//m�tudo existe apenas para confirmar se a conec��o foi efetuada, atrav�s de username, password, etc.
		if(conn == null) {
			conn = DriverManager.getConnection(url, username, password);
			stmt = conn.createStatement();
		}
	}
	
	public static ResultSet executeQuery(String sql) {//m�tudo para sacar informa��o da BD
		try {
			init();
			return stmt.executeQuery(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void execute(String sql) {//m�tudo para efetuar altera��es na BD
		try {
			init();
			stmt.execute(sql);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
}
