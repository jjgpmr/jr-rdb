package pt.rumos.repository;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Transaction;

public interface TransactionRepository {
	
	Optional<Transaction> save(Transaction transaction);

	//Optional<Transaction> update(Transaction transaction);

	List<Transaction> findAll();

	Optional<Transaction> findById(Long id);

//	void deleteById(Long id);

}
