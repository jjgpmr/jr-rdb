package pt.rumos.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.model.Client;

public class ClientRepositoryArrayImpl implements ClientRepository {

    private Client[] clients = new Client[3];
    private static long id = 0;

    // [{id:0, name:yuri, dob:04-12-1986}]
    // [{id:1, name:carlos, dob:04-12-1990}]
    // [{id:2, name:jose, dob:01-12-2000}]
    @Override
    public Optional<Client> save(Client client) {
        // Create method for unique NIF check
        for (int i = 0; i < clients.length; i++) {
            if (clients[i] == null) {
                client.setId(id);
                clients[i] = client;
                id++;
                return Optional.of(client);
            }
        }
        // Don't SYSO - Should be a LOG
        System.out.println("Database Full");
        return Optional.empty();
    }

    @Override
    public Optional<Client> update(Client client) {
        // Use method for unique NIF check
        for (int i = 0; i < clients.length; i++) {
            if (clients[i] != null) {
                Long existingClientId = clients[i].getId();
                Long updatingClientId = client.getId();
                if (existingClientId.equals(updatingClientId)) {
                    clients[i] = client;
                    return Optional.of(client);
                }
            }
        }
        // Don't SYSO - Should be a LOG
        System.out.println("Client not found");
        return Optional.empty();
    }

    @Override
    public List<Client> findAll() {
        return Arrays.asList(clients);
    }
    
    @Override
    public Optional<Client> findByNif(String nif) {
        for (int i = 0; i < clients.length; i++) {
            if (clients[i] != null) {
                String existingClientNif = clients[i].getNif();
                if (existingClientNif.equals(nif)) {
                    return Optional.of(clients[i]);
                }
            }
        }
        // Don't SYSO - Should be a LOG
        System.out.println("Client not found");
        return Optional.empty();
    }
    
    @Override
    public Optional<Client> findById(Long id) {
        for (int i = 0; i < clients.length; i++) {
            if (clients[i] != null) {
                Long existingClientId = clients[i].getId();
                if (existingClientId.equals(id)) {
                    return Optional.of(clients[i]);
                }
            }
        }
        // Don't SYSO - Should be a LOG
        System.out.println("Client not found");
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) {
        for (int i = 0; i < clients.length; i++) {
            if (clients[i] != null) {
                Long existingClientId = clients[i].getId();
                if (existingClientId.equals(id)) {
                    clients[i] = null;
                    // Don't SYSO - Should be a LOG
                    System.out.println("Client id:" + id + " deleted");                           
                    return;
                }
            }
        }
        throw new ResourceNotFoundException("Client not found");
    }

    @Override
    public void deleteByNIF(String nif) {
        for (int i = 0; i < clients.length; i++) {
            if (clients[i] != null) {
                String existingClientNIF = clients[i].getNif();
                if (existingClientNIF.equals(nif)) {
                    clients[i] = null;
                    // Don't SYSO - Should be a LOG
                    System.out.println("Client nif:" + nif + " deleted");
                    return;
                }
            }
        }
        throw new ResourceNotFoundException("NIF not found");
    }
    // private Optional<Client> findByName(String name) {
    // for (int i = 0; i < clients.length; i++) {
    // if (clients[i] != null) {
    // String existingClientName = clients[i].getName();
    // if (existingClientName.equalsIgnoreCase(name)) {
    // return Optional.of(clients[i]);
    // }
    // }
    // }
    // // Don't SYSO - Should be a LOG
    // System.out.println("Client not found");
    // return Optional.empty();
    // }
    

    

}
