package pt.rumos.repository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import pt.rumos.exception.ResourceNotFoundException;
import pt.rumos.model.Account;

public class AccountRepositoryArrayImpl implements AccountRepository {
	
	private Account[] accounts = new Account[2];
	private static Integer accountNumber = 0;

	@Override
	public void save(Account account) {
		// create method for unique accountNumber check
		for (int i=0; i<accounts.length; i++) {
			if(accounts[i] == null) {
				account.setAccountNumber(accountNumber);
				accounts[i] = account;//acrescentei uma account ao Array
				accountNumber++;//iterei o accountnumber
				//return Optional.of(account);//devolver a conta criada
			}
		}
        // Don't SYSO - Should be a LOG
        System.out.println("Database Full");//se ultrapassar por exemplo o array que s� est� para 2 index
       // return Optional.empty();
	}

	@Override
	public Optional<Account> update(Account account) {
		// use method for unique accountNumber check
		for(int i=0; i<accounts.length; i++) {
			if (accounts[i].getAccountNumber().equals(account.getAccountNumber())
					&& accounts[i].getAccountNumber() != null) {
				accounts[i] = account;
				return Optional.of(account);//anular uma account que seja igualmente null
			}
		}
		System.out.println("Account not found");//se n�o encontrou uma account com o accountnumber que procura, imprime isto.
		return Optional.empty();
	}

	@Override
	public List<Account> findAll() {
		return Arrays.asList(accounts);
	}

	@Override
	public Optional<Account> findById(Long id) {
		for(int i=0; i<accounts.length; i++) {
			if(accounts[i] !=null) {
				Long existingAccountId = accounts[i].getId();
				if(existingAccountId.equals(id)) {
				return Optional.of(accounts[i]);
				}
			}
		}
		System.out.println("Account id not found");
		return Optional.empty();
	}

	@Override
	public void deleteById(Long id) {
		for(int i=0; i<accounts.length; i++) {
			if(accounts[i] != null) {
				Long existingAccountId = accounts[i].getId();
				if(existingAccountId.equals(id)) {
					accounts[i] = null;
					System.out.println("Account id: " + id + "deleted");
					return;
				}
			}
		}
		throw new ResourceNotFoundException("Account id not found");
	}

	@Override
	public Optional<Account> findByAccountNumber(Integer accountNumber) {
		for(int i=0; i<accounts.length; i++) {
			if(accounts[i] != null) {
				Integer existingAccountNumber = accounts[i].getAccountNumber();
				if(existingAccountNumber.equals(accountNumber)) {
					return Optional.of(accounts[i]);
				}
			}
		}
		System.out.println("Account number not found");
		return Optional.empty();
	}

	@Override
	public void deleteByAccountNumber(Integer accountNumber) {
		for(int i=0; i<accounts.length; i++) {
			if(accounts[i] !=null) {
				Integer existingAccountNumber = accounts[i].getAccountNumber();
				if(existingAccountNumber.equals(accountNumber)) {
					accounts[i] = null;
					System.out.println("Account number:" + accountNumber + "deleted");
					return;
				}
			}
		}
		
		throw new ResourceNotFoundException("Account number not found");
	}
	
	

}
