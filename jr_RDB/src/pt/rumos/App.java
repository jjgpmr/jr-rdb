package pt.rumos;

import java.util.Scanner;

import pt.rumos.console.ATMApp;
import pt.rumos.console.ManagementApp;

public class App {

    // Única classe com MAIN <--
    // Ponto de início do código
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ManagementApp managementApp = ManagementApp.getInstance();
        managementApp.loadData();
        ATMApp atmApp = ATMApp.getInstance();
        
        int option = 0;
        do {
            System.out.println("Select an Option:");
            System.out.println("0: Exit");
            System.out.println("1: Management App");
            System.out.println("2: ATM App");
            option = scanner.nextInt();
            switch (option) {
            	case 0:
            		break;
                case 1:
                    managementApp.run();
                    break;
                case 2:
                    atmApp.run();
                    break;
                    
                default: 
            		System.out.println("Invalid option.");
            }

        } while (option != 0);

        System.out.println("Thanks for using RDB");
        System.out.println("********************");
        System.out.println("MAY THE FORCE BE WITH YOU!");

    }

}
