package pt.rumos.console;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import pt.rumos.model.Account;
import pt.rumos.model.Card;
import pt.rumos.model.Client;
import pt.rumos.model.CreditCard;
import pt.rumos.model.DebitCard;
import pt.rumos.model.Transaction;

public class ATMApp {
	//Todo o código de interação com o utilizador para ATM

	private static ATMApp instance;

	Scanner scanner = new Scanner(System.in);

	private ATMApp() {//m�todo construtor sem parametros
	} 

	public static ATMApp getInstance() {//m�todo singleton
		if(instance == null) {
			instance = new ATMApp();
		}
		return instance;
	}

	public void run() {

		Optional<Card> opCard = Optional.ofNullable(null);

		ConsoleUtils utils = new ConsoleUtils();
		utils.header("ATM Module");
		int option = 0;
		do {
			System.out.println("Select an Option:");
			System.out.println("0: Exit");
			System.out.println("1: Log in");
			option = scanner.nextInt();
			switch (option) {
			case 0:
				System.out.println("Thanks for using the ATM Module\n");
				return;
			case 1:
				System.out.println("Loggin in:");
				opCard = validLogin();
				break;
				
			default: 
        		System.out.println("Invalid option.");
			}
		}while (!opCard.isPresent());
		do {
			System.out.println("Select an Option:");
			System.out.println("1: Withdraw");
			System.out.println("2: Deposit");
			System.out.println("3: Transfer");
			System.out.println("4: Check Balance");
			System.out.println("5: Check Movements");
			option = scanner.nextInt();
			switch (option) {
			case 1:
				System.out.println("Withdrawing...");
				withdrawing(opCard.get());
				break;
			case 2:
				System.out.println("Depositing...");
				deposit(opCard.get());
				break;
			case 3:
				System.out.println("Transfering...");
				transfer(opCard.get());
				break;
			case 4:
				System.out.println("Checking balance...");
				checkBalance(opCard.get());
				break;
			}

		} while (option != 0);
		System.out.println("Thanks for using the ATM Module\n");

	}

	private Optional<Card> findCardfByCardNumber(String cardNumber) {
		ManagementApp app = ManagementApp.getInstance();
		List<Card> cards = app.getCards();
		List<Client> clients = app.getClients();
		for(Card c : cards) {
			if(cardNumber.equals(c.getCardNumber())) {
				return Optional.of(c);
			}
		}
		return Optional.ofNullable(null);
	}

	private Optional<Account> findAccountById(Long id){
		ManagementApp app = ManagementApp.getInstance();
		List<Account> accounts = app.getAccount();
		for(Account a : accounts) {
			if(id == a.getId()) {
				return Optional.of(a);
			}
		}
		return Optional.ofNullable(null);
	}

	private void withdrawing(Card card) {

		System.out.println("Enter amount:");
		Double amount = scanner.nextDouble();
		Account acc = card.getAccount();
		if(acc.getBalance() >= amount) {
			acc.setBalance(acc.getBalance() - amount);
			ManagementApp app = ManagementApp.getInstance();
			app.getAccountService().update(acc);
			if (card instanceof CreditCard) {
				int currNumWithdrawls = ((CreditCard) card).getNumberOfDailyWithdrawals();
				((CreditCard) card).setNumberOfDailyWithdrawals(currNumWithdrawls +1);
				app.getCardService().update(card);
				Transaction t = new Transaction();
				t.setAmountOfTransaction(amount*(-1));
				t.setDateOfTransaction(LocalDateTime.now());
				t.setOriginalAccount(acc);
				app.getTransactionService().save(t);
				
				
			} else {
				if (card instanceof DebitCard) {
					Double currAmountWithdrawls = ((DebitCard) card).getAmountOfDailyWithdrawals();
					((DebitCard) card).setAmountOfDailyWithdrawals(currAmountWithdrawls +1);
					app.getCardService().update(card);
					Transaction t = new Transaction();
					t.setAmountOfTransaction(amount*(-1));
					t.setDateOfTransaction(LocalDateTime.now());
					t.setOriginalAccount(acc);
					app.getTransactionService().save(t);
				}
				
			}
			System.out.println("Operation succedded!");
		}
		else {
			System.out.println("Insuficient funds.");
		}

	}

	// deposito na conta do cart�o do cliente que fez o login no ATM
	private void deposit(Card card) {

		System.out.println("Enter amount:");
		Double amount = scanner.nextDouble(); 
		Account acc = card.getAccount();
		acc.setBalance(acc.getBalance() + amount);
		ManagementApp app = ManagementApp.getInstance();
		try {
			Optional<Account> opAcc = app.getAccountService().update(acc);
			if(opAcc.isPresent()) {
				System.out.println("Operation succeeded!");
			}
			else {
				System.out.println("Operation went wrong.");
			}
		}catch(Exception e) {
			System.out.println("Operation went wrong.");
			System.out.println(e.getMessage());

		}

	}

	private void transfer(Card card) {

		ManagementApp app = ManagementApp.getInstance();
		Integer accountNumber;
		Account accountDestination;
		Double amount;

		System.out.println("Please choose the destination account number:");
		accountNumber = scanner.nextInt();
		accountDestination = app.getAccountByAccountNumber(accountNumber);
		System.out.println("Enter amount:");
		amount = scanner.nextDouble();
		deposit(accountDestination, amount);

		Account originalAccount = card.getAccount();
		originalAccount.setBalance(originalAccount.getBalance()-amount);
		try {
			Optional<Account> opAcc = app.getAccountService().update(originalAccount);
			if(opAcc.isPresent()) {
				System.out.println("Operation succeeded!");
			}
			else {
				System.out.println("Operation went wrong.");
			}
		}catch(Exception e) {
			System.out.println("Operation went wrong.");
			System.out.println(e.getMessage());

		}


	}


	private Optional<Card> validLogin() {

		String cardNumber;
		String pin;
		Optional<Card> opCard;
		Card c;

		scanner.nextLine();
		System.out.println("Insert Card Number");//para substituir o inserir o cart�o na m�quna de ATM...
		cardNumber = scanner.nextLine();

		opCard = findCardfByCardNumber(cardNumber); 
		if (opCard.isPresent()) {
			System.out.println("Please enter PIN:");
			pin = scanner.nextLine();	
			c = opCard.get();
			if(pin.equals(c.getPinNumber())) {
				return opCard;
			}
			else{
				System.out.println("Pin is invalid.");
				return Optional.ofNullable(null);
			}

		}
		else {
			System.out.println("Card number is invalid.");
			return Optional.ofNullable(null);
		}	
	}

	private void deposit(Account account, Double amount) {//este m�todo serve para depositar o valor recebido do parametro amount, na conta recebida no primeiro parametro

		ManagementApp app = ManagementApp.getInstance();

		account.setBalance(account.getBalance() + amount);
		try {
			Optional<Account> opAcc = app.getAccountService().update(account);
			if(!opAcc.isPresent()) {
				System.out.println("Operation went wrong.");
			}
		}catch(Exception e) {
			System.out.println("Operation went wrong.");
			System.out.println(e.getMessage());
		}

	}

	private void checkBalance(Card card){
		
		Account acc = card.getAccount();
		System.out.println("Your balance is: " + acc.getBalance());
	}

}

