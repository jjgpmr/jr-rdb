package pt.rumos.console;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Scanner;

import pt.rumos.model.Account;
import pt.rumos.model.Card;
import pt.rumos.model.Client;
import pt.rumos.model.CreditCard;
import pt.rumos.model.DebitCard;
import pt.rumos.model.Transaction;
import pt.rumos.repository.AccountRepositorySQLImpl;
import pt.rumos.repository.CardRepositorySQLImpl;
import pt.rumos.repository.ClientRepositorySQLImpl;
import pt.rumos.repository.SecondaryOwnersRepositorySQLImpl;
import pt.rumos.repository.TransactionRepositorySQLImpl;
import pt.rumos.service.AccountService;
import pt.rumos.service.AccountServiceImpl;
import pt.rumos.service.CardService;
import pt.rumos.service.CardServiceImpl;
import pt.rumos.service.ClientService;
import pt.rumos.service.ClientServiceImpl;
import pt.rumos.service.SecondaryOwnersService;
import pt.rumos.service.SecondaryOwnersServiceImpl;
import pt.rumos.service.TransactionService;
import pt.rumos.service.TransactionServiceImpl;

public class ManagementApp {
    // Todo o c�digo de intera��o com o utilizador para a gest�o de:
    // Clientes, Contas e Cartões
	
	private static ManagementApp INSTANCE = null;
    private static final Integer DCARD_MAX_WIDTHDRAWALS_PER_DAY = 400;
    private static final Integer DCARD_MAX_AMOUNT_PER_TRANS = 200;
    
    
	
    private ClientService clientService = new ClientServiceImpl(new ClientRepositorySQLImpl());
    private AccountService accountService = new AccountServiceImpl(new AccountRepositorySQLImpl());
    private CardService cardService = new CardServiceImpl(new CardRepositorySQLImpl());
    private SecondaryOwnersService secondaryOwnersService = new SecondaryOwnersServiceImpl(new SecondaryOwnersRepositorySQLImpl());
    private TransactionService transactionService = new TransactionServiceImpl(new TransactionRepositorySQLImpl());
    private List <Client> clients = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List <Card> cards = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();
    
    Scanner scanner = new Scanner(System.in);

    private ManagementApp() {//m�todo construtor sem parametros
    
    }
    
    public static ManagementApp getInstance() {//m�todo singleton
    	if(INSTANCE == null) {
    		INSTANCE = new ManagementApp();
    	}
    	return INSTANCE;
    }
    
    public void run() {
    	
    	Optional<Client> opClient = Optional.ofNullable(null);
    	
        ConsoleUtils utils = new ConsoleUtils();
        utils.header("Management Module");
        int option = 0;
        do {

        	System.out.println("Select an Option:");
        	System.out.println("0: Exit");
        	System.out.println("1: Log in:");
        	option = scanner.nextInt();
        	switch (option) {
        	case 0:
        		System.out.println("Thanks for using the Management Module");
        		return;
        	case 1:
        		System.out.println("Loggin in...");
        		opClient = validLogin();
        		break;
        	
        	default: 
        		System.out.println("Invalid option.");
        	}
        	
        }while (!opClient.isPresent());
        if(opClient.get().getUserType()=='c') {
        	ATMApp atm = ATMApp.getInstance();
        	
        }
        
        do {
            showOptionsMenu();
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Managing clients...");
                    do {
                        System.out.println("Select an Option:");
                        System.out.println("0: Exit");
                        System.out.println("1: Add Client");
                        System.out.println("2: List All Clients");
                        System.out.println("3: Show Client By NIF");
                        System.out.println("4: Delete Client By NIF");
                        option = scanner.nextInt();
                        switch (option) {
                            case 1:
                                System.out.println("Adding client...");
                                addClient();
                                break;
                            case 2:
                                for (Client c : clients) {//for para iterar a lista de clientes, para n�o aparecerem todos de seguida na mesma lista.
                                	System.out.println("Printing Client Number " + c.getId() + "-------------------------------");//s� para ficar mais bonito.
                                	System.out.println(c);
                                }
                                break;
                            case 3:
                                System.out.println("Showing client by NIF...");
                                findByNif();
                                break;
                            case 4:
                                System.out.println("Deleting client by NIF...");
                                deleteByNIF();
                                break;
                                
                        }

                    } while (option != 0);
                    break;
                case 2:
                    System.out.println("Managing accounts...");
				option = manageAccount();
                    break;
                case 3:
                    System.out.println("Managing cards...");
                    do {
                    	System.out.println("Select an option:");
                    	System.out.println("0: Exit");
                    	System.out.println("1: Add Credit Card");
                    	System.out.println("2: Add Debit Card");
                    	System.out.println("3: List cards");
                    	System.out.println("4: Show Card by Card Number");
                    	System.out.println("5: Delete Card by Card Number");
                    	option = scanner.nextInt();
                    	switch (option) {
                    	case 1: 
                    		System.out.println("Adding Credit card:");
                    		addCard('c');
                    		break;
                    	case 2: 
                    		System.out.println("Adding Debit card:");
                    		addCard('d');
                    		break;
                    	case 3:
                    		for(Card c : cards) {
                    			if(c instanceof CreditCard) {
                    				System.out.println((CreditCard)c);
                    			}else {
                    				System.out.println((DebitCard)c);
                    			}

                    		}
                    	case 4:
                    		System.out.println("Showing Card by card number:");
                    		findByCardNumber();
                    		break;	
                    	case 5:
                    		System.out.println("Deleting:");
                    		deleteByCardNumber();
                    	}
                    } while (option !=0);
                    break;
            }

        } while (option != 0);
        System.out.println("Thanks for using the Management Module");
    }
    
    public void loadData() {
    	loadClients();
    	loadAccounts();
    	loadCards();
    	loadSecondaryOwners();
    }
    
    private void loadClients() {
    	clients = clientService.getAll();
    }
    
    public Client getClientById(Long id) {
    	
    	for (Client client : clients) {
    		if(client.getId() == id) {
    			return client;
    		}
    	}
    	return null;
    }
    
    
    private Optional<Client> findClientfByContractNumber(String contractNumber) {
		for(Client c : clients) {
			if(contractNumber.equals(c.getContractNumber())) {
				return Optional.of(c);
			}
		}
		return Optional.ofNullable(null);
	}
    
    private void loadAccounts() {
    	accounts = accountService.getAll();
    }
    
    public Account getAccountById(Long id) {
    	for(Account account : accounts) {
    		if(account.getId() == id) {
    			return account;
    		}
    	}
    	return null;
    }
  
    
    public Account getAccountByAccountNumber(Integer accountNumber) {
    	for(Account account : accounts) {
    		if(account.getAccountNumber().equals(accountNumber)) {
    			return account;
    		}
    	}
		return null;
    }
    
    private void loadSecondaryOwners() {
    	secondaryOwnersService.getAll();
    }
    
    private void loadCards() {
    	cards = cardService.getAll();
    }
    
	private void findByCardNumber() {
		System.out.println("\nCard Number");
		System.out.println(cardService.findByCardNumber(scanner.nextLine()));
	}																						

	private void deleteByCardNumber() {
		System.out.println("\nCard Number");
		cardService.deleteByCardNumber(scanner.nextLine());
		System.out.println("\nCard deleted.");
	}																				

    public Card getCardById(Long id) {
    	for(Card card : cards) {
    		if(card.getId() == id) {
    			return card;
    		}
    	}
    	return null;
    }
    
    private int countNumberOfCards(char cardType, Client holder) {
    	
    	int numOfCards = 0;
    	
    	for(Card card: cards) {
    		if (card.getHolder().getId() == holder.getId()) {
    			
    			if (cardType == 'd' && card instanceof DebitCard)
    				numOfCards ++; 
    			else if (cardType == 'c' && card instanceof CreditCard) {
    				numOfCards ++; 
    			}
    		}
    		
    	}	
    	return numOfCards;
    }
    

    private int countNumberOfAccountofClient(Client holder) {
    	
    	int numOfAcc = 0;
    	
    	for(Account acc: accounts) {
    		if (acc.getOwner().getId() == holder.getId()) {
    			numOfAcc ++;
    			
    		}
    		
    	}	
    	return numOfAcc;
    }
    
    
    
    
    private void addCard(char cType) {
    	Card card ;
    	int digit;
    	char option;
    	char cardType;
    	int numberOfCards;
    	int numOfAcc;
    	String cardNumber;
    	Integer accountNumber;
    	Account accNumber;
    	String pin = "";
    	System.out.println("Holder:");
    	System.out.println("Please select Client Id:");
    	for(Client client : clients) {
    		System.out.println(client.getId() + " " + client.getName());
    	}
    	Long holderId = scanner.nextLong();
    	Client holder = clientService.getById(holderId);
    	numOfAcc = countNumberOfAccountofClient(holder);
    	if (numOfAcc == 0) {
    		System.out.println("Client has no accounts");
    		return;
    	}
    	
    	
    	numberOfCards = countNumberOfCards(cType,holder);
    	
    	if (cType == 'd' && numberOfCards >= 1) {
    		System.out.println("Client can have only one debit card");
    		return;
    	}
    	
    	if (cType == 'c' && numberOfCards >= 2) {
    		System.out.println("Client can have only two credit cards");
    		return;
    	}
    	
    	System.out.println("Please select the account:");
    	
    	for(Account acc : accounts) {
    		if (acc.getOwner().getId() == holder.getId()) {
    			System.out.println(acc.getAccountNumber());
    		}	
    	}
    	accountNumber = scanner.nextInt();
    	accNumber = getAccountByAccountNumber(accountNumber);
    	
    	Random rand = new Random();
    	
    	for (int i = 0; i < 4; i++) {
    		
    	  digit = rand.nextInt(10);
    	  pin = pin + Integer.toString(digit); 		  
    	}
    	
    	boolean validCardNumber = true;
    	
    	do {
    		validCardNumber = true;
    		System.out.println("Please enter the Card Number");
    		cardNumber = scanner.next();
    		int size = cardNumber.length();
    		if (size != 4) {
    			System.out.println("Card Number must have 4 digits."); 
    			validCardNumber = false;
    		}
    		for (int i = 0; i < size; i++) {
    			if (!Character.isDigit(cardNumber.charAt(i))) {
    				System.out.println("Card Number can only have digits");
    				validCardNumber = false;
    				break;
    			}
    		}		
    			
    	} while (!validCardNumber);
    				
    	
    	
    	
    	if (cType == 'c') {
    		System.out.println("Please insert the card plafond:");
    		Double plafond = scanner.nextDouble();
    		CreditCard cCard = new CreditCard();
    		cCard.setAccount(accNumber);
    		cCard.setPinNumber(pin);
    		cCard.setHolder(holder);
    		cCard.setNumberOfDailyWithdrawals(0);
    		cCard.setFirstTimeLogin(1);
    		cCard.setCardNumber(cardNumber);
    		cCard.setPlafond(plafond);
    		Optional<Card> opCard = cardService.save(cCard);
    	    if(opCard.isPresent()) {
    	    	cards.add(cCard);
    	    	System.out.println("Operation succeded");
    	    	
    	    }
    	    else {
    	    	System.out.println("Error, please contact the administrator");
    	    }
    	} else {   // debit card
    		DebitCard dCard = new DebitCard();
      		dCard.setAccount(accNumber);
      		dCard.setPinNumber(pin);
      		dCard.setHolder(holder);
      		dCard.setAmountOfDailyWithdrawals(0.0);
      		dCard.setFirstTimeLogin(1);
      		dCard.setCardNumber(cardNumber);
      		Optional<Card> opCard = cardService.save(dCard);
      	    if(opCard.isPresent()) {
      	    	cards.add(dCard);
      	    	System.out.println("Operation succeded");
      	    }
      	    else {   	    
      	    	System.out.println("Error, please contact the administrator");
      	    }  
    	  }
    		  
	}
    
    public CardService getCardService() {
    	return cardService;
    }
    
    public List<Card> getCards() {
    	return cards;
    }

	private int manageAccount() {
		int option;
		do {
			System.out.println("Select an option:");
			System.out.println("0: Exit");
		    System.out.println("1: Create Account");
		    System.out.println("2: List All Accounts");
		    System.out.println("3: Show Account By Account Number");
		    System.out.println("4: Delete Account By Account Number");
		    option = scanner.nextInt();
		    switch (option) {
		    case 1:
		    	System.out.println("Creating Account...");
		    	createAccount();
		    	break;
		    case 2:
		    	for(Account acc : accounts) {
		    		System.out.println(acc);
		    	}
		    	break;
		    case 3:
		    	System.out.println("Please select Account Number:");
		    	Long id = scanner.nextLong();
		    	Account createdAccount = accountService.getById(id);
		    	System.out.println(createdAccount);
		    	break;
		    case 4:
		    	System.out.println("Deleting account:");
		    	deleteByAccountNumber();
		    }
		}while (option != 0);
		return option;
	}
	
	public AccountService getAccountService() {
		return accountService;
	}
	
	public List<Account> getAccount(){
		return accounts;
	}
	
	public void deleteByAccountNumber() {
		boolean error = false;
		System.out.println("\nAccount Number:");
		Integer accNumber = scanner.nextInt();
		
		try {
		   accountService.deleteByAccountNumber(accNumber);
		} catch (RuntimeException e) {
			System.out.println("Account can�t be deleted because, please verify if there is data associated with this account.");
			error = true;
		}
		if (!error) {
			System.out.println("Account deleted.");
			Account acc = getAccountByAccountNumber(accNumber);
			accounts.remove(acc);
		}	
	}

    private void showOptionsMenu() {
        System.out.println("Select an Option:");
        System.out.println("0: Exit");
        System.out.println("1: Clients");
        System.out.println("2: Accounts");
        System.out.println("3: Cards");
    }

    private void addClient() {
        Client c = new Client();
        scanner.nextLine();
        System.out.println("NIF:");
        c.setNif(scanner.nextLine());
        System.out.println("\nPassword:");
        c.setPassword(scanner.nextLine());
        System.out.println("\nName:");
        c.setName(scanner.nextLine());
        System.out.println("\nDate of birth:");
        String dateOfBirth = scanner.nextLine();
        DateTimeFormatter dob = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        c.setDob(LocalDate.parse(dateOfBirth, dob));
        System.out.println("\nTelephone:");
        c.setTelephone(scanner.nextLine());
        System.out.println("\nMobile:");
        c.setMobile(scanner.nextLine());
        System.out.println("\nEmail:");
        c.setEmail(scanner.nextLine());
        System.out.println("\nOccupation:");
        c.setOccupation(scanner.nextLine());
       
       Optional<Client> opClient = clientService.save(c);
        if(opClient.isPresent()) {
        	clients.add(c);
        }
        clients.add(null);
    }
    
    public ClientService getClientService() {
    	return clientService;
    	
    }
    public List <Client> getClients(){
    	return clients;
    }
    
    public TransactionService getTransactionService() {
    	return transactionService;
    }
    
    private Account createAccount() {
    	Account account = new Account();
    	char option;
    	System.out.println("Owner:");
    	System.out.println("Please select Client Id:");
    	for (Client c : clients) {
    		System.out.println(c.getId() + " " + c.getName());
    	}
    	
    	Long ownerId = scanner.nextLong();
    	Client owner = clientService.getById(ownerId);
    	if (owner != null) {
    		account.setOwner(owner);
    		System.out.println("Please enter the Account number:");
    		Integer accountNumber = scanner.nextInt();
    		account.setAccountNumber(accountNumber);
    		System.out.println("Please insert initial balance:");
    		Double balance = scanner.nextDouble();
    		if(balance >= 50) {
    			
    			accounts.add(null);
    		
    			System.out.println("Account created successfully!");
    		} else {
    			System.out.println("Minimum balance is 50�.");
    		}
    	}else {
    		System.out.println("Wrong Client ID!!!");
    	}
    	
    	
		return account;
    }

    private void findByNif() {//Clientes
        System.out.println("\nNIF:");
        System.out.println(clientService.findByNif(scanner.nextLine()));

    }

    private void deleteByNIF() {//Clientes
    	System.out.println("\nNif");
		clientService.deleteByNIF(scanner.nextLine());
		System.out.println("\nClient deleted.");
    }
    
//    boolean error = false;
//	System.out.println("\nAccount Number:");
//	Integer accNumber = scanner.nextInt();
//	
//	try {
//	   accountService.deleteByAccountNumber(accNumber);
//	} catch (RuntimeException e) {
//		System.out.println("Account can�t be deleted because, please verify if there is data associated with this account.");
//		error = true;
//	}
//	if (!error) {
//		System.out.println("Account deleted.");
//		Account acc = getAccountByAccountNumber(accNumber);
//		accounts.remove(acc);
//	}	
//    
    private Optional<Client> validLogin() {
    	
    	String contractNumber;
    	String password;
    	Optional<Client> opClient;
    	Client c;
    	
    	scanner.nextLine();
    	System.out.println("Insert contract number or username:");//contract number � para o client, e o username � para o funcion�rio do banco.
    	contractNumber = scanner.nextLine();
    	opClient = findClientfByContractNumber(contractNumber); 
    	if(opClient.isPresent()) {
    		System.out.println("Please enter password:");
    		password = scanner.nextLine();
    		c = opClient.get();
    		if(password.equals(c.getPassword())) {
    			return opClient;
    		}
    		else {
    			System.out.println("Password is invalid.");
    			return Optional.ofNullable(null);
    		}
    	}
    	else {
    		System.out.println("Invalid credentials.");
			return Optional.ofNullable(null);
    	}
		
    }

}
