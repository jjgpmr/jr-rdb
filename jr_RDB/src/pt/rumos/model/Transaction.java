package pt.rumos.model;

import java.time.LocalDateTime;

public class Transaction {
	
	private Long id;
	private LocalDateTime dateOfTransaction;
	private Double amountOfTransaction;
	private Account originalAccount;
	private Account destinationAccount;
	
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public LocalDateTime getDateOfTransaction() {return dateOfTransaction;}
	public void setDateOfTransaction(LocalDateTime dateOfTransaction) {this.dateOfTransaction = dateOfTransaction;}
	
	public Double getAmountOfTransaction() {return amountOfTransaction;}
	public void setAmountOfTransaction(Double amountOfTransaction) {this.amountOfTransaction = amountOfTransaction;}
	
	public Account getOriginalAccount() {return originalAccount;}
	public void setOriginalAccount(Account originalAccount) {this.originalAccount = originalAccount;}
	
	public Account getDestinationAccount() {return destinationAccount;}
	public void setDestinationAccount(Account destinationAccount) {this.destinationAccount = destinationAccount;}
	
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", dateOfTransaction=" + dateOfTransaction + ", amountOfTransaction="
				+ amountOfTransaction + ", originalAccount=" + originalAccount + ", destinationAccount=" + destinationAccount + "]";
	}
	
	

}
