package pt.rumos.model;

public class CreditCard extends Card{

	private Integer numberOfDailyWithdrawals;

	public Integer getNumberOfDailyWithdrawals() {return numberOfDailyWithdrawals;}
	public void setNumberOfDailyWithdrawals(Integer numberOfDailyWithdrawals) {this.numberOfDailyWithdrawals = numberOfDailyWithdrawals;}
	
	@Override
	public String toString() {
		return "CreditCard [numberOfDailyWithdrawals=" + numberOfDailyWithdrawals + ", getHolder()=" + getHolder().getName()
				+ ", getAccount()=" + getAccount().getAccountNumber() + ", getCardNumber()=" + getCardNumber() + ", getPinNumber()="
				+ getPinNumber() + "]";
	}
}
