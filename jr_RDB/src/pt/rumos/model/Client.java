package pt.rumos.model;

import java.time.LocalDate;

public class Client {

    private Long id;
    private String nif;
    private String password;
    private String name;
    private LocalDate dob;
    private String telephone;
    private String mobile;
    private String email;
    private String occupation;
    private String contractNumber;
    private char userType;
    
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getNif() { return nif; }
    public void setNif(String nif) { this.nif = nif; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public LocalDate getDob() { return dob; }
    public void setDob(LocalDate dob) { this.dob = dob; }

    public String getTelephone() { return telephone; }
    public void setTelephone(String telephone) { this.telephone = telephone; }

    public String getMobile() { return mobile; }
    public void setMobile(String mobile) { this.mobile = mobile; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getOccupation() { return occupation; }
    public void setOccupation(String occupation) { this.occupation = occupation; }
    
    public String getContractNumber() {return contractNumber;}
	public void setContractNumber(String contractNumber) {this.contractNumber = contractNumber;}
	
	public char getUserType() {return userType;}
	public void setUserType(char userType) {this.userType = userType;}
	
	@Override
    public String toString() {
        return "Client Id=" + id + ", Nif=" + nif + ", Password=" + password + ", Name=" + name + ", Dob=" + dob + ", Telephone=" + telephone
                + ", Mobile=" + mobile + ", Email=" + email + ", Occupation=" + occupation + ", Contract Number" + contractNumber + ", User Type=" + userType;
    }
}