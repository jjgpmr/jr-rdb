package pt.rumos.model;

public class DebitCard extends Card {

	private Double amountOfDailyWithdrawals;

	public Double getAmountOfDailyWithdrawals() {return amountOfDailyWithdrawals;}
	public void setAmountOfDailyWithdrawals(Double amountOfDailyWithdrawals) {this.amountOfDailyWithdrawals = amountOfDailyWithdrawals;}
	
	@Override
	public String toString() {
		return "DebitCard [amountOfDailyWithdrawals=" + amountOfDailyWithdrawals + ", getHolder()=" + getHolder().getName()
				+ ", getAccount()=" + getAccount() + ", getCardNumber()=" + getCardNumber() + ", getPinNumber()="
				+ getPinNumber() + "]";
	}
}
