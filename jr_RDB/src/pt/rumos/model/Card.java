package pt.rumos.model;

public class Card {

	private Long id;
	private Client holder;
	private Account account;
	private String cardNumber;
	private String pinNumber;
	private Integer firstTimeLogin;
	private Double plafond;
	
	public Long getId() {return id;}
	public void setId(Long id) {this.id = id;}
	
	public Client getHolder() {return holder;}
	public void setHolder(Client holder) {this.holder = holder;}
	
	public Account getAccount() {return account;}
	public void setAccount(Account account) {this.account = account;}
	
	public String getCardNumber() {return cardNumber;}
	public void setCardNumber(String cardNumber) {this.cardNumber = cardNumber;}
	
	public String getPinNumber() {return pinNumber;}
	public void setPinNumber(String pinNumber) {this.pinNumber = pinNumber;}
	
	public Integer getFirstTimeLogin() {return firstTimeLogin;}
	public void setFirstTimeLogin(Integer firstTimeLogin) {this.firstTimeLogin = firstTimeLogin;}
	
	public Double getPlafond() {return plafond;}
	public void setPlafond(Double plafond) {this.plafond = plafond;}
	
	@Override
	public String toString() {
		return "Id=" + id + ", Card holder=" + holder.getName() + ", Account=" + account.getAccountNumber() + ", Card Number=" + cardNumber + ", Pin Number="
				+ pinNumber;
	}
	
}