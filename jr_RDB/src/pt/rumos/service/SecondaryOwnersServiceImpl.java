package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;
import pt.rumos.repository.SecondaryOwnersRepository;
import pt.rumos.repository.SecondaryOwnersRepositorySQLImpl;

public class SecondaryOwnersServiceImpl implements SecondaryOwnersService {
	
	private SecondaryOwnersRepository secondaryOwnersRepository;
	
	public SecondaryOwnersServiceImpl(SecondaryOwnersRepository secondaryOwnersRepository) {
		this.secondaryOwnersRepository = secondaryOwnersRepository;
	}
	
	public SecondaryOwnersServiceImpl() {
		this.secondaryOwnersRepository =  new SecondaryOwnersRepositorySQLImpl();
	}

	@Override
	public Optional<Client> save(Long id_client, Long id_account) {
		return null;

	}

	@Override
	public Optional<Client> update(Long id_client, Long id_account, Long new_id_client, Long new_id_account) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void getAll() {
		
		secondaryOwnersRepository.findAll();
	}

	@Override
	public void deleteByIds(Long id_client, Long id_account) {
		// TODO Auto-generated method stub

	}

}
