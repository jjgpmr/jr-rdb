package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Account;

public interface AccountService {
	
	Optional<Account> save(Account account);
	
	Optional<Account> update(Account account);
	
	List<Account> getAll();
	
	Account getById(Long id);
	
	void deleteById(Long id);
	
	Account findByAccountNumber(Integer accountNumber);
	
	void deleteByAccountNumber(Integer accountNumber);
}
