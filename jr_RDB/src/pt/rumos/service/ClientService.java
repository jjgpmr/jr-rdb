package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;

public interface ClientService {

   Optional<Client> save(Client client);

   Optional<Client> update(Client client);

    List<Client> getAll();

    Client getById(Long id);

    void deleteById(Long id);
    
    Client findByNif(String nif);
    
    void deleteByNIF(String nif);
}
