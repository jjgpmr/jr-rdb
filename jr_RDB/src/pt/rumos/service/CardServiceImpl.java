package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Card;
import pt.rumos.repository.CardRepository;

public class CardServiceImpl implements CardService {
	
	private CardRepository cardRepository;
	
	public CardServiceImpl(CardRepository cardRepository) {
		this.cardRepository = cardRepository;
	}

	@Override
	public Optional<Card> save(Card card) {
		
		return cardRepository.save(card);

	}

	@Override
	public Card update(Card card) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Card> getAll() {
		return cardRepository.findAll();
	}

	@Override
	public Card getById(Long id) {
		return cardRepository.findById(id).get();
	}

	@Override
	public void deleteById(Long id) {
		cardRepository.deleteById(id);
	}

	@Override
	public Card findByCardNumber(String cardNumber) {
		
		return cardRepository.FindByCardNumber(cardNumber).get();
	}

	@Override
	public void deleteByCardNumber(String cardNumber) {
		cardRepository.deleteByCardNumber(cardNumber);

	}

}
