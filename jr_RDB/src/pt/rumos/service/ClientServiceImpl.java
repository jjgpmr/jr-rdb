package pt.rumos.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;
import pt.rumos.repository.ClientRepository;
import pt.rumos.repository.ClientRepositorySQLImpl;

public class ClientServiceImpl implements ClientService {//esta classe serve para p�r as regras e as l�gicas de neg�cio. Classe conv�m ser abstracta, para conseguir reutilizar c�digo.

    private ClientRepository clientRepository;

    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }
    
    public ClientServiceImpl(){
    	this.clientRepository = new ClientRepositorySQLImpl();
    }

    @Override
    public Optional<Client> save(Client client) {
        boolean isUnderage = isUnderage(client);
        
       return clientRepository.save(client);
/*
        Optional<Client> savedClient = clientRepository.save(client);
        if (savedClient.isPresent()) {
            // Saved
            return savedClient.get();
        } else {
            // Not Saved
            throw new RuntimeException("Client was not saved");
        }
        */

    }

    private boolean isUnderage(Client client) {
        Integer yearOfBirth = client.getDob().getYear();
        Integer currentYear = LocalDate.now().getYear();
        Integer age = currentYear - yearOfBirth;
        if (age < 18) {
            return true;
        }
        return false;
    }

    @Override
    public Optional<Client> update(Client client) {
        // Must perform 'isPresent' check...
        return Optional.of(clientRepository.update(client).get());
    }

	@Override
	public List<Client> getAll() {
		return clientRepository.findAll();
	}

    @Override
    public Client getById(Long id) {
        // Must perform 'isPresent' check...
        return clientRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public void deleteByNIF(String nif) {
        clientRepository.deleteByNIF(nif);
    }

    @Override
    public Client findByNif(String nif) {
        return clientRepository.findByNif(nif).get();
    }

}
