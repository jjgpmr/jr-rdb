package pt.rumos.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pt.rumos.model.Account;
import pt.rumos.repository.AccountRepository;
import pt.rumos.repository.AccountRepositorySQLImpl;

public class AccountServiceImpl implements AccountService {
	
	private AccountRepository accountRepository;
	
	public AccountServiceImpl(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	public AccountServiceImpl() {
		this.accountRepository =  new AccountRepositorySQLImpl();
	}

	@Override
	public Optional<Account> save(Account account) {
		
		 return accountRepository.save(account);
	}

	@Override
	public Optional<Account> update(Account account) {
		return Optional.ofNullable(accountRepository.update(account).get());
	}

	@Override
	public List<Account> getAll() {
		return accountRepository.findAll();
	}

	@Override
	public Account getById(Long id) {
		return accountRepository.findById(id).get();
	}

	@Override
	public void deleteById(Long id) {
		accountRepository.deleteById(id);
		
	}

	@Override
	public Account findByAccountNumber(Integer accountNumber) {
		return accountRepository.findByAccountNumber(accountNumber).get();
	}

	@Override
	public void deleteByAccountNumber(Integer accountNumber) {
		accountRepository.deleteByAccountNumber(accountNumber);
		
	}

	

	
}
