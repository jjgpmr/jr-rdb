package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Transaction;

public interface TransactionService {

	Optional<Transaction> save(Transaction transaction);

	//Optional<Transaction> update(Transaction transaction);

	List<Transaction> getAll();
	
	Transaction getById(Long id);

	//void deleteById(Long id);

	Transaction findById(Long id);

}
