package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Transaction;
import pt.rumos.repository.TransactionRepository;
import pt.rumos.repository.TransactionRepositorySQLImpl;

public class TransactionServiceImpl implements TransactionService {

	private TransactionRepository transactionRepository;

	public TransactionServiceImpl(TransactionRepository transactionRepository) {
		this.transactionRepository = transactionRepository;
	}

	public TransactionServiceImpl() {
		this.transactionRepository = new TransactionRepositorySQLImpl();
	}

	@Override
	public Optional<Transaction> save(Transaction transaction) {
		
		return transactionRepository.save(transaction);
	}

	//@Override
//	//public Optional<Transaction> update(Transaction transaction) {//as transactions n�o podem ser alteradas, por ningu�m.
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public List<Transaction> getAll() {
		
		return transactionRepository.findAll();
	}

	@Override
	public Transaction getById(Long id) {
		
		return transactionRepository.findById(id).get();
	}

//	@Override
//	public void deleteById(Long id) {
//		transactionRepository.deleteById(id);
//
//	}

	@Override
	public Transaction findById(Long id) {
	
		return transactionRepository.findById(id).get();
	}

}
