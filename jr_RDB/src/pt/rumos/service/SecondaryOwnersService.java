package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Client;

public interface SecondaryOwnersService {

	Optional<Client> save(Long id_client, Long id_account);
	
	Optional<Client> update(Long id_client, Long id_account, Long new_id_client, Long new_id_account);
	
	void getAll();
	
	void deleteByIds(Long id_client, Long id_account);
	
	
}
