package pt.rumos.service;

import java.util.List;
import java.util.Optional;

import pt.rumos.model.Card;

public interface CardService {
	
	Optional<Card> save(Card card);
	
	Card update(Card card);
	
	List<Card> getAll();
	
	Card getById(Long id);
	
	void deleteById(Long id);
	
	Card findByCardNumber(String cardNumber);
	
	void deleteByCardNumber(String cardNumber);

}
